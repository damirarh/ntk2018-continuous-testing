using Library;
using System;
using Xunit;

namespace Tests
{
    public class FibonacciTest
    {
        [Fact]
        public void Calculate1()
        {
            var fibonacci = new FibonacciService();
            Assert.Equal(1, fibonacci.Calculate(1));
        }

        [Fact]
        public void Calculate8()
        {
            var fibonacci = new FibonacciService();
            Assert.Equal(21, fibonacci.Calculate(8));
        }

        [Fact]
        public void CalculateOutOfRange()
        {
            var fibonacci = new FibonacciService();
            Assert.Throws<ArgumentOutOfRangeException>(() => fibonacci.Calculate(-1));
        }
    }
}
